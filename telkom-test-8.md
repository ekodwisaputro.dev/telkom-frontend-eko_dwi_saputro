## a.

| Field Name    | Data Type | Length/Size | Index       | Extra          |
|---------------|-----------|-------------|-------------|----------------|
| id_anggota    | BIGINT    | 20          | Primary Key | auto_increment |
| nik           | Int       | 16          |             |                |
| nama_anggota  | Varchar   | 30          |             |                |
| tempat_lahir  | Varchar   | 30          |             |                |
| tanggal_lahir | Date      |             |             |                |
| pekerjaan     | Varchar   | 30          |             |                |
| jenis_kelamin | TinyInt   | 1           |             |                |
| id_ayah       | BigInt    | 20          |             |                |
| id_ibu        | BigInt    | 20          |             |                |




## b.

Kelebihannya
-   table yang saya buat mudah dibaca dan dipahami


kekurangannya adalah hanya manggunakan 1 table, untuk membuat suatu aplikasi dapat ditambahkan
table pendukungnya. seperti menambahakan table pasangan.


## c.

```sql
SELECT gen1.nama_anggota as 'Generasi 1',
       gen2.nama_anggota as 'Generasi 2',
       gen3.nama_anggota as 'Generasi 3',
       gen4.nama_anggota as 'Generasi 4',
       gen5.nama_anggota as 'Generasi 5'

FROM anggota_keluarga as gen1
INNER JOIN anggota_keluarga as gen2 ON gen2.id_ayah=gen1.id_anggota
INNER JOIN anggota_keluarga as gen3 ON gen3.id_ayah=gen2.id_anggota
INNER JOIN anggota_keluarga as gen4 ON gen4.id_ayah=gen3.id_anggota
INNER JOIN anggota_keluarga as gen5 ON gen5.id_ayah=gen4.id_anggota
WHERE gen1.id_anggota = '1'
