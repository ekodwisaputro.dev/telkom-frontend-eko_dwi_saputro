Unit test menggunakan PHPUnit

```php
<?php
  public function user_can_create_user(){
    $user = factory(User::class)->create();
    $this->assertEquals($user->name, $user->getName());
    $this->assertEquals(1, count($user));
  }
?>
