Keuntungan :
- Dapat mengeksekusi tanpa harus menunggu operasi sebelumnya selesai dieksekusi, sehingga menghasilkan aplikasi web yang lebih cepat dan efisien.
- Kecepatan I/O jauh cepat dari kecepatan prosesor dalam memproses data.


Kerugian :

Kekurangannya adalah pada saat menggunakan Cron job akan bermasalah ketika pada proses yang besar
dan membutuhkan waktu yang cepat dan kadang bisa terjadi ‘offside pemrosessan’ proses yang pertama kali belum selesai sudah disusul proses yang selanjutnya.
Queue jobs juga harus mengantri terlebih dahulu, hal ini akan sangat menganggu kenyamanan user.
