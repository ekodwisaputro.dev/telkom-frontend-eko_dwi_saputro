var items = [14, 8, 3, 22, 9];

function swap(items, firstIndex, secondIndex){
  var temp = items[firstIndex];
  items[firstIndex] = items[secondIndex];
  items[secondIndex] = temp;
}

function selectionSort(items){

    var len = items.length, min;
    var mulai = 1;


    for (i=0; i < len; i++){

        //set posisi minimal
        min = i;

        //cek array ada yg kecil atau tidak
        for (j=i+1; j < len; j++){
            if (items[j] < items[min]){
                min = j;
            }
        }

        //jika posisi salah di swap
        if (i != min){
            swap(items, i, min);

            console.log('proses swap ' +mulai+ ': ' +items);
            var selesai = mulai;
            mulai++;

        }

    }


    return selesai;
}

console.log(selectionSort(items));
